/*
	vx8restore.c
	Restore a Yaesu VX-8DR ham radio.
	ems oct2010
	
VX-8DR: hold down [Fn] while powering on, press [Mode] to receive memory
VX-8DR operates at 38400/N81, and sends 10 bytes, such as:
	4148323944-0000010100 "AH29D"
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/select.h>
#include <string.h>

char *bin_filename;

#define BUF_SIZE 100000

int main(int argc, char *argv[]) {
	int fd;
	struct termios io;
	fd_set readfds, writefds, errorfds;
	struct timeval timeout;
	int ret;
	int image_size = 0;
	int bytes_in, total_bytes_in = 0,
		bytes_out, total_bytes_out = 0;
	/* int i;*/
	unsigned char image[BUF_SIZE];
	unsigned char inbuf[10];
	FILE *bin;
	
	int flags;
	
	
	if(argc == 2) {
		bin_filename = argv[1];
	} else {
		fprintf(stderr,"vx8restore [flash-image-filename]\n");
		exit(1);
	}

	printf("Hold down [F]W while turning on radio to activate clone mode.\n"
	"When CLONE appears, plug in serial cable...\n");
	
	while( (fd = open("/dev/cu.usbserial", O_RDWR|O_EXLOCK|O_NONBLOCK)) == -1) {
		sleep(1);
	}
	printf("USB Serial interface detected.\n"
	"Press [MODE] on radio then press Enter here...\n");
	getc(stdin);
	printf("Talking to radio...\n");

	if( ! (bin = fopen(bin_filename, "r"))) {
		fprintf(stderr,"Error opening backup file for reading.\n");
		exit(1);
	}
	
	while( (bytes_in=fread(image+image_size,1,100000,bin)) )
		image_size += bytes_in;
	fclose(bin);
	printf("%d bytes read in from %s\n", image_size, bin_filename);

	flags = fcntl(fd, F_GETFL, 0);
	fcntl(fd, F_SETFL, flags | O_NONBLOCK);	

	tcgetattr(fd, &io);
	cfsetspeed(&io, B38400);	/* 38400 baud */
	cfmakeraw(&io);
	
	/* input flags */
	io.c_iflag &= ~ISTRIP;
	io.c_iflag &= ~IXON;
	io.c_iflag &= ~IXOFF;

/*	io.c_iflag &= ~ISIG; 
	io.c_iflag &= ~IEXTEN; 
	io.c_iflag |= IGNBRK;
	io.c_iflag |= IGNPAR; 
*/
	/* control flags */	
	io.c_cflag &= ~PARENB;  /* no parity */
	io.c_cflag &= ~PARODD;  /* even parity */
	io.c_cflag &= ~CSIZE;	
	io.c_cflag |= CS8;	/* 8 data bits */
	io.c_cflag &= ~CSTOPB;	/* 1 stop bit */

	io.c_cflag |= CLOCAL;	/* ignore modem control lines like DSR/DTR */
	io.c_cflag &= ~HUPCL;	/* ignore line hangups */
	io.c_cflag |= CREAD;	/* enable receiver */

	io.c_cflag &= ~CRTSCTS;	/* No RTS/CTS hardware flow control */
	io.c_cflag &= ~CDTR_IFLOW; /* Ignore DTR */
	io.c_cflag &= ~CDSR_OFLOW; /* Ignore DSR */
	io.c_cflag &= ~CCAR_OFLOW; /* Ignore DCD */
	
	/* output flags */
	io.c_oflag &= ~OPOST;	/* No output processing */
	
	/* local flags */
	io.c_lflag &= ~ICANON;	/* Ignore "special" input characters */
	io.c_lflag &= ~ISIG;
	io.c_lflag &= ~ECHOCTL;

	if( tcsetattr(fd, TCSAFLUSH, &io) ) {
		printf("tcsetattr() failed.\n");
	}
	
	sleep(1);
	printf("Beginning initiation handshake with radio.\n");
	bytes_out = write(fd, image, 10);
	total_bytes_out += bytes_out;
	
	
	while(1) {
		/* printf("Waiting for input...\n");*/
		
		FD_ZERO(&readfds);
		FD_ZERO(&writefds);
		FD_ZERO(&errorfds);
		FD_SET(fd, &readfds);
		//FD_SET(fd, &writefds);
		//FD_SET(fd, &errorfds);
		
		timeout.tv_sec = 10;
		timeout.tv_usec = 0;
		ret = select(fd+1, &readfds, &writefds, &errorfds, &timeout);
		
		if( ret == 1 ) {
			if( FD_ISSET(fd, &errorfds) ) {
				printf("select(): Error on serial line.\n");
			}
			
			if( FD_ISSET(fd, &readfds) ) {
				bytes_in = read(fd, inbuf+total_bytes_in, BUF_SIZE);
				total_bytes_in += bytes_in;

				/*
				printf("From radio: ");
				for(i = 0; i < total_bytes_in ; i++) printf("%02x.", inbuf[i]);
				printf("\n");
				*/
				
				if(inbuf[total_bytes_in-1] == 0x06) {
					printf("Radio acknowledged. Sending image.\n");
					
					/* go back to blocking I/O */
					int flags = fcntl(fd, F_GETFL, 0);
					fcntl(fd, F_SETFL, flags & ~O_NONBLOCK);
					
					bytes_out = write(fd, image+10, image_size - 10);
					total_bytes_out += bytes_out;
					if(total_bytes_out == image_size) {
						printf("Done! %d bytes restored to radio.\n", total_bytes_out);
					} else {
						printf("Error! Incorrect amount of data (%d bytes instead of %d) written out! Radio memory may have been erased!\n",
							total_bytes_out, image_size);
					}
					close(fd);
					
					exit(0);
				}
			}
		}
		         
	}
	
	return 0;
}
