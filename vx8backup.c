/*
	vx8backup.c
	Backup a Yaesu VX-8DR ham radio.
	ems oct2010
	
VX-8DR: hold down [Fn] while powering on, press [Band] to transmit memory
VX-8DR operates at 38400/N81, and sends 10 bytes, such as:
	4148323944-0000010100 "AH29D"
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/select.h>
#include <string.h>

char *vx_8dr_id = "AH29D";
char bin_filename[128];
int image_size = 65227;

#define BUF_SIZE 100000

int main() {
	int fd;
	struct termios io;
	fd_set readfds, writefds, errorfds;
	struct timeval timeout;
	int ret;
	int bytes_in, total_bytes_in = 0, last_print_bytes = 0;
	unsigned char buffer[BUF_SIZE];
	unsigned char outbuf[10];
	FILE *bin;
	int start = 0;

	printf("Hold down [F]W while turning on radio to activate clone mode.\n"
	"When CLONE appears, plug in serial cable...\n");
	
	while( (fd = open("/dev/cu.usbserial", O_RDWR|O_EXLOCK|O_NONBLOCK)) == -1) {
		sleep(1);
	}
	printf("USB Serial interface detected.\n"
	"Press [BAND] on radio to begin download...\n");
	
	fcntl(fd, F_SETFL, O_NONBLOCK);
	tcgetattr(fd, &io);
	cfsetspeed(&io, B38400);	/* 38400 baud */
	cfmakeraw(&io);
	
	/* input flags */
	io.c_iflag &= ~ISTRIP;
	io.c_iflag &= ~IXON;
	io.c_iflag &= ~IXOFF;

/*	io.c_iflag &= ~ISIG; 
	io.c_iflag &= ~IEXTEN; 
	io.c_iflag |= IGNBRK;
	io.c_iflag |= IGNPAR; 
*/
	/* control flags */	
	io.c_cflag &= ~PARENB;  /* no parity */
	io.c_cflag &= ~PARODD;  /* even parity */
	io.c_cflag &= ~CSIZE;	
	io.c_cflag |= CS8;	/* 8 data bits */
	io.c_cflag &= ~CSTOPB;	/* 1 stop bit */

	io.c_cflag |= CLOCAL;	/* ignore modem control lines like DSR/DTR */
	io.c_cflag &= ~HUPCL;	/* ignore line hangups */
	io.c_cflag |= CREAD;	/* enable receiver */

	io.c_cflag &= ~CRTSCTS;	/* No RTS/CTS hardware flow control */
	io.c_cflag &= ~CDTR_IFLOW; /* Ignore DTR */
	io.c_cflag &= ~CDSR_OFLOW; /* Ignore DSR */
	io.c_cflag &= ~CCAR_OFLOW; /* Ignore DCD */
	
	/* output flags */
	io.c_oflag &= ~OPOST;	/* No output processing */
	
	/* local flags */
	io.c_lflag &= ~ICANON;	/* Ignore "special" input characters */
	io.c_lflag &= ~ISIG;
	io.c_lflag &= ~ECHOCTL;

	if( tcsetattr(fd, TCSAFLUSH, &io) ) {
		printf("tcsetattr() failed.\n");
	}
	
	while(1) {
		/* printf("Waiting for input...\n");*/
		
		FD_ZERO(&readfds);
		FD_ZERO(&writefds);
		FD_ZERO(&errorfds);
		FD_SET(fd, &readfds);
		//FD_SET(fd, &writefds);
		//FD_SET(fd, &errorfds);
		
		timeout.tv_sec = 10;
		timeout.tv_usec = 0;
		ret = select(fd+1, &readfds, &writefds, &errorfds, &timeout);
		
		if( ret == 1 ) {
			if( FD_ISSET(fd, &errorfds) ) {
				printf("select(): Error on serial line.\n");
			}
			
			if( FD_ISSET(fd, &readfds) ) {
				bytes_in = read(fd, buffer+total_bytes_in, BUF_SIZE);
				total_bytes_in += bytes_in;
				
				if(start) {
					if((total_bytes_in-last_print_bytes) >= 512) {
						last_print_bytes = total_bytes_in;
						printf("%6d bytes loaded [%c%c%c%c%c%c%c%c%c%c]\r",
							total_bytes_in,
							total_bytes_in>=(0*image_size/10)?'#':'_',
							total_bytes_in>=(1*image_size/10)?'#':'_',
							total_bytes_in>=(2*image_size/10)?'#':'_',
							total_bytes_in>=(3*image_size/10)?'#':'_',
							total_bytes_in>=(4*image_size/10)?'#':'_',
							total_bytes_in>=(5*image_size/10)?'#':'_',
							total_bytes_in>=(6*image_size/10)?'#':'_',
							total_bytes_in>=(7*image_size/10)?'#':'_',
							total_bytes_in>=(8*image_size/10)?'#':'_',
							total_bytes_in>=(9*image_size/10)?'#':'_'
						);
						fflush(stdout);
					}
				
					//printf("%d bytes saved:\n", total_bytes_in);
					if(total_bytes_in == image_size) {
						sprintf(bin_filename,"%s.bin", vx_8dr_id);
						if( ! (bin = fopen(bin_filename, "w"))) {
							fprintf(stderr,"Error opening backup file for writing.\n");
							exit(1);
						} else {
							fwrite(&buffer,1,total_bytes_in,bin);
							fflush(bin);
							printf("\nRadio backup saved to %s.\n", bin_filename);
							fclose(bin);
							
							exit(0);
						}
					}
				} else {
					if(total_bytes_in >= 10 &&
					   buffer[total_bytes_in-10+0] == vx_8dr_id[0] &&
					   buffer[total_bytes_in-10+1] == vx_8dr_id[1] &&
					   buffer[total_bytes_in-10+2] == vx_8dr_id[2] &&
					   buffer[total_bytes_in-10+3] == vx_8dr_id[3] &&
					   buffer[total_bytes_in-10+4] == vx_8dr_id[4]) {
					   	printf("Radio ready to transmit. Software version: %c%c%c%c%c (%d.%d.%d.%d.%d)\n",
					   		buffer[total_bytes_in-10+0],
					   		buffer[total_bytes_in-10+1],
					   		buffer[total_bytes_in-10+2],
					   		buffer[total_bytes_in-10+3],
					   		buffer[total_bytes_in-10+4],
					   		buffer[total_bytes_in-10+5],
					   		buffer[total_bytes_in-10+6],
					   		buffer[total_bytes_in-10+7],
					   		buffer[total_bytes_in-10+8],
					   		buffer[total_bytes_in-10+9]
					   	);

						start = 1;
				   	
						printf("Beginning transfer\n");
						outbuf[0] = 0x06;
						write(fd, outbuf, 1);
						
						if(total_bytes_in > 10) {
							total_bytes_in = 10;
							memmove(buffer,buffer+total_bytes_in-10,10);
						}
					}
				}
			}
		}
		         
	}
	
	close(fd);
	
	fclose(bin);

	return 0;
}
