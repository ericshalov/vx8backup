/*
	vx8mem.c
	Manage Yaesu VX-8DR memories in flash image file.
	ems oct2010
	
	11.25.3b.38.2c.33.36.32.29.24.1d.33.3b.29.36
	Hawthorne Tower
	
	11.25.3b.38.2c.33.36.32.29.24.10.32.28
	Hawthorne Gnd
	
	11.25.3b.38.2c.33.36.32.29.24.0a.1d.12.1c
	Hawthorne ATIS
	
	ff=NULL
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* A 100-character character set used by Yaesu (chars 1-9 unused) */
char yaesu_charset[256] = {
	'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F',
	'G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V',
	'W','X','Y','Z',' ','a','b','c','d','e','f','g','h','i','j','k',
	'l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','.',
	',',':',';','*','#','_','-','/','&','(',')','@','!','?','^','X',
	'0','1','2','3','4','5','6','7','8','9'
};

char *filename;
unsigned char buffer[100000];

#define MAX_NAME_LEN 16

#define MODULATION_FM  0x00
#define MODULATION_AM  0x40
#define MODULATION_WFM 0x80

int main(int argc, char *argv[]) {
	FILE *f;
	int bytes_in, total_bytes_in=0;
	int i, mem;
	char name[MAX_NAME_LEN+1];
	float freq;
	int modulation;
	
	
	if(argc == 2) {
        	filename = argv[1];
	} else {
		fprintf(stderr,"vx8mem [flash-image-filename]\n");
		exit(1);
	}
                                                                        	
	
	if( (f=fopen(filename,"r")) ) {
		while( (bytes_in=fread(buffer+total_bytes_in,1,100000,f)) )
			total_bytes_in += bytes_in;
		fclose(f);
		//printf("%d bytes read in from %s\n", total_bytes_in, filename);
	}

	for(mem=0;mem<1100;mem++) {
		freq = 100.0 * (buffer[0x328A+mem*32+2]>>4)
		     +  10.0 * (buffer[0x328A+mem*32+2]&0xF)
		     +   1.0 * (buffer[0x328A+mem*32+3]>>4)
		     +        ((buffer[0x328A+mem*32+3]&0xF) / 10.0)
		     +        ((buffer[0x328A+mem*32+4]>>4)   / 100.0)
		     +        ((buffer[0x328A+mem*32+4]&0xF) / 1000.0);
		
		if(buffer[0x328A+mem*32+1] & MODULATION_WFM) modulation = MODULATION_WFM;
		else if(buffer[0x328A+mem*32+1] & MODULATION_AM) modulation = MODULATION_AM;
		else modulation = MODULATION_FM;
	

		// for(i=0;i<32;i++) printf("%c", buffer[0x328A+mem*32+i]);
		//printf("\n");

		/*
		for(i=0;i<32;i++) printf("%02x.", buffer[0x328A+mem*32+i]);
		printf("\n");
		*/
		
		/* Convert name from Yaesu charset */
		memset(name,'\0',MAX_NAME_LEN+1);
		for(i=0;i<MAX_NAME_LEN;i++) name[i] = buffer[0x328A+mem*32+i+8] == 0xFF ? '\0' :
			yaesu_charset[buffer[0x328A+mem*32+i+8]];
			
		printf("%d,\"%s\",%0.3f,%-3s\n",
			mem+1, name, freq,
			modulation == MODULATION_AM  ? "AM" :
			modulation == MODULATION_FM  ? "FM" :
			modulation == MODULATION_WFM ? "WFM" :
			"???"
		);
	}
	
	return 0;
}
