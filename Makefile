CC = gcc
CFLAGS = -Wall
RM = /bin/rm

BINS = vx8backup vx8restore vx8mem

default: $(BINS)

clean:
	$(RM) -f $(BINS)
all: $(BINS)

vx8mem: vx8mem.c
	$(CC) $(CFLAGS) -o vx8mem vx8mem.c

vx8backup: vx8backup.c
	$(CC) $(CFLAGS) -o vx8backup vx8backup.c

vx8restore: vx8restore.c
	$(CC) $(CFLAGS) -o vx8restore vx8restore.c
