<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Yaesu-VX8R.JPG/220px-Yaesu-VX8R.JPG" align="right" />

vx8backup
=========
A Linux command-line tool for backing up the memories in a Yaesu VX-8DR amateur handheld tranceiver.

The VX-8DR is an advanced Multi‑Band HT capable of transmitting on 4 bands
(50,144,220,440), and includes a built-in AX.25-capable APRS modem.  The
radio also offers optional GPS and Bluetooth interfaces.

With an impressive internal memory for storing frequencies, backing up the
radio's frequency memory is essential.

Prerequisites
-------------
To use these tools, you will need:
1) A Yaesu VX-8DR radio. (It hasn't been tested on anything else, and probably won't work.)

2) A Yaesu radio-USB interface cable.

3) A Linux system.

Downloads:
----------
Clone the repo:

$ git clone 'https://github.com/EricShalov/vx8backup.git'

Instructions:
-------------
To backup your radio:
* Turn off the radio.
* Plug in your USB adaptor cable to your computer
* Plug the radio into the other end of the cable
* Hold down [Fn] while powering on the radio
* Start vx8backup at the command-line.
* Press [Band] to transmit the radio's memory to the computer.

To restore your radio from a backup produced by "vx8backup":

* Turn off the radio.
* Plug in your USB adaptor cable to your computer
* Plug the radio into the other end of the cable
* Hold down [Fn] while powering on the radio
* Press [Mode] to get the radio to receive it's memories from your computer.
* Start vx8restore at the command-line.


License:
--------
"vx8backup" is licensed under the <a href="http://opensource.org/licenses/BSD-3-Clause">BSD 3-Clause License</a>.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the {organization} nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.
